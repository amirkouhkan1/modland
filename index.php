<?php
    include('./config/conf.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="دانلود بازی‌های مود شده">
    <meta name="keywords" content="android, Android, apk, game, mod, data, game mod">
    <meta name="author" content="Amir Kouhkan">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="static/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/style.css">
    <script src="static/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <title>صفحه‌ی اصلی | بازی‌های mod شده</title>
</head>
<body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
        <article>
            <h2 class="text-center mt-5">لیست بازی‌ها</h2>

            <!-- Action game -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">اکشن</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'action');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=action" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>
            
            <!-- Board game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">تخته‌ای</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'board');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=board" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- Word game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">بازی با کلمات</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'word');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=word" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- Strategy game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">استراتژی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'strategy');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=strategy" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- arcade game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">آرکِید</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'arcade');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=arcade" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- card game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">کارتی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'card');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=card" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- casual game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">تصادفی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'casual');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=casual" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- casino game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">کازینو</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'casino');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=casino" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- puzzle game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">پازل</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'puzzle');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=puzzle" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- racing game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">مسابقه‌ای</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'racing');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=racing" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- sports-games game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">ورزشی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'sports-games');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=sports-games" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- simulation game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">شبیه‌سازی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'simulation');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=simulation" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- music game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">موسیقی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'music');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=music" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>

            <!-- educational game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">آموزشی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'educational');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=educational" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>
            <!-- puzzle game  -->
            <div class="row">
                <div class="col-sm-12">
                        <div class="card m-4">
                        <div class="card-header">استراتژی</div>
                        <div class="row">
                            <?php
                                $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category LIMIT 6");
                                $statement->bindvalue(':category', 'puzzle');
                                $statement->execute();
                                $result = $statement->fetchAll();
                                foreach($result as $row){
                            ?>
                                <div class="col-sm-4">
                                    <div class="card bg-light">
                                        <img class="card-img-top p-1" style="width: 120px; height: 120px; margin-right: 105px;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4>
                                            <p class="card-text"><?php echo $row['company']; ?></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                        </div>
                        <div class="card-footer text-center"><a href="game.php?category=puzzle" class="btn btn-primary">بیشتر</a></div>
                    </div>
                </div>
            </div>
        </article>
    </div>

    <?php include('./templates/footer.html'); ?>
</body>
</html>