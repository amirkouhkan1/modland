<nav class="navbar navbar-expand-sm bg-dark navbar-dark" style="direction: rtl;">
    <!-- Links -->
    <ul class="navbar-nav p-3">
        <li class="navbar-brand">مود لند</li>
        <li class="nav-item mr-4">
            <a class="nav-link" href="index.php">خانه</a>
        </li>
        <li>
            <a class="nav-link" href="about.php">درباره</a>
        </li>
    </ul>

    <form class="form-inline" method="post" action="game.php">
        <input class="form-control mr-sm-2" name="txtSearch" type="text" placeholder="جستجو">
        <input type="submit" value="جستجو" name="btnSearch" class="btn btn-success mr-2">
    </form>
</nav>