<?php
    include('./config/conf.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="دانلود بازی‌های مود شده">
    <meta name="keywords" content="android, Android, apk, game, mod, data, game mod">
    <meta name="author" content="Amir Kouhkan">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="static/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/style.css">
    <script src="static/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <title>صفحه‌ی اصلی | بازی‌های mod شده</title>
</head>
<body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
        <article class="m-5">
            <?php 
                if(isset($_GET['game_id'])){
                    $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE id=:id");
                    $statement->bindvalue(':id', $_GET['game_id']);
                    $statement->execute();
                    $result = $statement->fetchAll();
                    foreach($result as $row){
                ?>
                                <div class="col-sm-12">
                                    <div class="card bg-light text-left">
                                        <img class="card-img-top p-1 mx-auto" style="height: 20rem; width: 18rem;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4> <span><?php echo $row['company']; ?></span>
                                            <p class="card-text"></p>
                                            <p class="lead"><?php echo $row['description'];?></p>
                                            <a href="<?php echo $row['apk']; ?>" class="btn btn-primary btn-block">دانلود بازی</a>
                                            <a href="<?php echo $row['data']; ?>" class="btn btn-primary btn-block">دانلود دیتا</a>
                                        </div>
                                    </div>
                                </div>
                        <?php }} else if($_GET['category']){
                    $statement = $myPDO->prepare("SELECT * FROM tbl_game WHERE category=:category");
                    $statement->bindvalue(':category', $_GET['category']);
                    $statement->execute();
                    $result = $statement->fetchAll();
                    foreach($result as $row){
                ?>
                                <div class="col-sm-12">
                                    <div class="card bg-light text-left">
                                        <img class="card-img-top p-1 mx-auto" style="height: 20rem; width: 18rem;" src="<?php echo $row["icon"]; ?>" alt="icon game">
                                        <div class="card-body">
                                            <h4 class="card-title"><?php echo $row['title']; ?></h4> <span><?php echo $row['company']; ?></span>
                                            <p class="card-text"></p>
                                            <a href="game.php?game_id=<?php echo $row['id']; ?>" class="btn btn-primary btn-block">دیدن بازی</a>
                                        </div>
                                    </div>
                                </div>

                    <?php }} else if(isset($_POST['btnSearch'])){
                        $search = htmlspecialchars($_POST['txtSearch']);
                        if(empty($search)){
                            echo "چیزی جستجو نکردید";
                        }else{
                            echo "بخش جستجوی این سایت هنوز تکمیل نشده است.";
                        }
                        ?>                        
                        <?php }else{?>
                        <p class="mt-5 text-center">چیزی برای نمایش وجود ندارد</p>
                    <?php } ?>
            
        </article>
    </div>
    <?php include('./templates/footer.html'); ?>
</body>
</html>