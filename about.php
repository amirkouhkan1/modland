<?php
    include('./config/conf.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="دانلود بازی‌های مود شده">
    <meta name="keywords" content="android, Android, apk, game, mod, data, game mod">
    <meta name="author" content="Amir Kouhkan">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="static/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/style.css">
    <script src="static/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <title>صفحه‌ی اصلی | بازی‌های mod شده</title>
</head>
<body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
        <h3 class="mt-4 text-center">درباره</h3>    
    <article class="m-5">
            <p class="text-right" style="direction: rtl;">این وب‌سایت بخشی از وب‌لاگ من (<a href="http://liniolon.ir">مشاهده‌ی وب‌لاگ</a>) هست. بخاطر علاقه‌ی خودم به بازی‌های مود شده تصمیم گرفتم این وب‌سایت رو برای علاقه‌مندان به بازی‌های راحت‌الحلقوم بسازم.</p>
            <p class="text-right" style="direction: rtl;">امیدوارم که لذت ببرید</p>
            <p class="text-right" style="direction: rtl;">وب‌سایت هنوز خام هست و مشکلات امنیتی زیادی داره :) از اینکه قدرت نفوذ خودتون رو روی این سایت امتحان نمی‌کنید متشکرم :)</p>
            <p class="text-right" style="direction: rtl;">ارتباط با من از طریق ایمیل: amirkouhkan1@gmail.com</p>
            <p class="text-right" style="direction: rtl;">به‌زودی برای مشارکت همگانی روی گیت‌هاب و گیت‌لب قرار می‌گیرد :)</p>
        </article>
    </div>
    <?php include('./templates/footer.html'); ?>
</body>
</html>